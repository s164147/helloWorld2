# new feature
# Tags: optional

Feature: Transfer money between two accounts
  Actor: Bank

  Scenario: Transfer money successfully
    Given There is two persons registered in the system with identifier "e68b7542-ea4b-4048-9776-a2741b8c8ff3", "02fdb47d-af40-4a97-b970-1602625e8d9f", balance 100, 0
    When Identifer " " transfers 100 to account with identifier " " and message "Happy Birthday Jimmy"
    Then Identifier " " will have an account balance on 0, and identifier " " will have an account balance on 100

    #Alle dem under her skal lige skrives op


  Scenario: Transfer money unsuccessfully because missing identifier for destination
    Given I am registered in the bank with identifier "" and have a balance on 100
    When I transfer 100 with the  message "Happy Birthday Jimmy"
    Then I would get an error message with "Creditor account does not exist"

  Scenario: Transfer money unsuccessfully because missing message
    Given I am registered in the bank with identifier "" and have a balance on 100
    When I transfer 100 to account with identifier " "
    Then I would get an error message with "Description missing"

  Scenario:Transfer money unsuccessfully because missing identifier for source
    Given I am registered in the bank and have a balance on 100
    When I transfer 100 to account with identifier " ", and message "Happy Birthday Jimmy"
    Then I would get an error message with "Debtor account does not exist"

  Scenario: Transfer money unsuccessfully because i send negative money
    Given I am registered in the bank with identifier "" and have a balance on 100
    When I transfer negative amount -100 to account with identifier " ", and message "Happy Birthday Jimmy"
    Then  I would get an error message with "Amount must be positive"

  Scenario: Transfer money unsuccessfully because i would get a negative balance
    Given I am registered in the bank with identifier "" and have a balance on 100
    When I transfer the amount 101 to account with identifier " ", and message "Happy Birthday Jimmy"
    Then I would get the error message "Debtor balance will be negative"