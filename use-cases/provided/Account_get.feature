Feature: Finding an Account
  Description: Find an account with an identifier.
  Actors: User


  Scenario: Finding an account successfully
    When I search for an account with the identifier " "
    Then The account is returned

  Scenario: Finding an account unsuccessfully
    When I search for a non existing account with the identifier "thisuserdoesnotexist"
    Then I get error message "Account does not exist"