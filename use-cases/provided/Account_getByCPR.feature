Feature: Finding an Account with CPR number
  Description: Find an account with a CPR number.
  Actors: User


  Scenario: Finding an account successfully
    When I search for an account with the CPR "200000-2222"
    Then I get the account returned

  Scenario: Finding an account unsuccessfully
    When I search for the non existing account with the CPR "101194-1234"
    Then I get an error message with "Account does not exist"
