Feature: Merchant requests payment
  Description: Merchant wants to initiate a transaction with a customer
  Actors: Merchant


  Scenario: Merchant requests payment from a valid customer
    Given I want to request a payment from customer who has a valid token and enough money
    When I scan the customers token and request payment of 100 kr
    Then I should get the money and get a success message "You have received 100 kr"