Feature: Deleting an Account
  Description: Delete an account with an identifier.
  Actors: Bank


  Scenario: Deleting an account successfully
    When I delete an existing account
    Then I should get "Account does not exist"

  Scenario: Deleting an account unsuccessfully
    When I delete a non existing account
    Then I get error message with "Account does not exist"