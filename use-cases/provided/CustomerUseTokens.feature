Feature: Customer wants to use a token

  Background: Customer is registered
    Actor: Customer

    Scenario: I want to use a unused token
      Given I have 1 unused token
      When Merchant scans my token
      Then I lose my token
      Then The Token is deleted in the database

  Scenario: I want to use a used token
    Given I have 1 used token
    When Merchant scans my token
    Then I lose my token
    Then I get an error message "The token is not available in the system"

  Scenario: I want to use a token, while having none
    Given I have 0 used token
    Then I get an error message "The token is not available in the system"