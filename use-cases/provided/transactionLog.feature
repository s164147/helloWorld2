Feature: Logging of transactions

When Whenever a transaction occurs it must be logged

  Background: A transaction can occur

  Scenario: Customer Transaction update
    Given The transaction has been verified during the sale
    When Transaction occurs
    Then Customer views the transaction log to see if the recent sale has been updated

  Scenario: Merchant Transaction update
    Given The transaction has been verified during the sale
    When Transaction occurs
    Then Merchant views the transaction log to see if the recent sale has been updated

  Scenario: The merchant wants to view the transaction log
    Given The merchant can access the log
    When The merchant want to view the log too see money deposits and token received
    Then Merchant accesses the log

  Scenario: The customer wants to view the transaction log
    Given The customer can access the log
    When The customer want to view the log to see money withdrawn and token used
    Then Customer accesses the log