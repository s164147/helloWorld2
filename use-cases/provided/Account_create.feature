Feature: Create an Account
  Description: A new account is created
  Actors: User


  Scenario: Creating an account successfully
    When I make a new account with name "Henrik",  lastname "Larsen", CPR "112233-1111" and bank deposit on 100
    Then the account should be added to the database

  Scenario: Creating an account unsuccessfully with a used CPR
    When I make a new account with name "Ole",  lastname "Olsen", same CPR "100000-1111" and bank deposit on 100
    Then Error message occurs "Account already exists"

  Scenario: Creating an account unsuccessfully with a negative initial balance
    When I make a new account with name "Hanne",  lastname "Koefod", CPR "112233-1212" and bank deposit on negative balance
    Then I would get "Initial balance must be 0 or positive"
