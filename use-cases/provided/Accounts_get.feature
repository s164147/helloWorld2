Feature: Get a list of all accounts in the bank with all the information
  Actor: Bank

  Scenario: I want to see a list of all accounts.
    When I search for all the accounts in the bank.
    Then I get a list of all the accounts in the bank.