Feature: Refund

  The customer needs to be able to get a refund on a transaction.

  Background: The customer requesting a refund exists

    Scenario:  The customer requests a refund on a transaction with an existing merchant
      Given The merchant exist
      And The presented token is valid
      When The customer requests a refund
      Then The customer will be refunded

    Scenario: The customer requests a refund on a transaction where the merchant no longer exists
      Given The merchant no longer exists
      And The presented token is valid
      When The customer requests a refund
      Then The customer will not be refunded

    Scenario: The customer requests a refund with an invalid token
      Given The merchant exist
      And The token is invalid
      When The customer requests a refund
      Then The customer will not be refunded