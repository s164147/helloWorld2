package testing.dtu;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TransactionLog {
    @Given("^The transaction has been verified during the sale$")
    public void the_transaction_has_been_verified_during_the_sale() throws Exception {
        // Write code here that turns the phrase above into concrete actions

    }

    @When("^Transaction occurs$")
    public void transaction_occurs() throws Exception {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^Customer views the transaction log to see if the recent sale has been updated$")
    public void customer_views_the_transaction_log_to_see_if_the_recent_sale_has_been_updated() throws Exception {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^Merchant views the transaction log to see if the recent sale has been updated$")
    public void merchant_views_the_transaction_log_to_see_if_the_recent_sale_has_been_updated() throws Exception {
        // Write code here that turns the phrase above into concrete actions

    }

    @Given("^The merchant can access the log$")
    public void the_merchant_can_access_the_log() throws Exception {
        // Write code here that turns the phrase above into concrete actions

    }

    @When("^The merchant want to view the log too see money deposits and token received$")
    public void the_merchant_want_to_view_the_log_too_see_money_deposits_and_token_received() throws Exception {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^Merchant accesses the log$")
    public void merchant_accesses_the_log() throws Exception {
        // Write code here that turns the phrase above into concrete actions

    }

    @Given("^The customer can access the log$")
    public void the_customer_can_access_the_log() throws Exception {
        // Write code here that turns the phrase above into concrete actions

    }

    @When("^The customer want to view the log to see money withdrawn and token used$")
    public void the_customer_want_to_view_the_log_to_see_money_withdrawn_and_token_used() throws Exception {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^Customer accesses the log$")
    public void customer_accesses_the_log() throws Exception {
        // Write code here that turns the phrase above into concrete actions

    }
}
