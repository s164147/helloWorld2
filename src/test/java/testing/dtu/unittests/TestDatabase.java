package testing.dtu;

import CustomExceptions.TokenRequestDeniedException;
import code.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestDatabase {

    Customer a = new Customer("Joe","1");
    Customer b = new Customer("Bob","2");
    Customer c = new Customer("Alice","3");

    Customer customerList[] = {a,b,c};
    TokenManager tokenmngr = new TokenManager();

    DatabaseController testDatabase = DatabaseController.getInstance();
    @Test
    public void testInsertandExtract(){
        testDatabase.addUser(a);
        assertEquals(a.getName(),testDatabase.findUser("1").getName());

    }

    @Test
    public void testSearch(){
        for(Customer customer : customerList){
            testDatabase.addUser(customer);
        }
        for(Customer customer : customerList){
            assertEquals(customer.getName(),testDatabase.findUser(customer.getCpr()).getName());
        }
    }
    @Test
    public void testRemove(){
        for(Customer customer : customerList){
            testDatabase.addUser(customer);
        }
        testDatabase.removeUser(customerList[0].getCpr());
        assertNull(testDatabase.findUser(customerList[0].getCpr()));
    }

    @Test
    public void testInsertAndExtractToken(){
        try {
            tokenmngr.createTokens(5,a.getCpr());
        } catch (TokenRequestDeniedException e) {
            e.printStackTrace();
        }
        Token token;
        for(int i = 0; i<5;i++){
            token = tokenmngr.getToken();
            testDatabase.addToken(token);
            assertEquals(true,testDatabase.tokenExists(token.getTokenString()));
        }

    }
    @Test
    public void testRemoveToken(){
        try {
            tokenmngr.createTokens(1, a.getCpr());
        } catch (TokenRequestDeniedException e) {
            e.printStackTrace();
        }
        Token token = tokenmngr.getToken();
        testDatabase.addToken(token);
        testDatabase.removeToken(token);
        assertFalse(testDatabase.tokenExists(token.getTokenString()));
    }
}
