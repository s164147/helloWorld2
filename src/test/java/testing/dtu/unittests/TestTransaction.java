package testing.dtu.unittests;
import code.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TestTransaction {
    Merchant merchant = new Merchant("Mad","1010");
    Customer customer = new Customer("Joe","1");

    @Test
    public void testAddRemoveMoney(){
        try {
            customer.account.addMoney(100);
        } catch (Exception invalidAmount) {
            fail("Adding money threw an invalid amount exception.");
        }
        try {
            customer.account.removeMoney(50);
        } catch (Exception invalidAmount) {
            fail("Removing money threw an invalid amount exception.");
        }
        assertEquals(150,customer.account.getBalance(),0);
    }
    @Test
    public void testTransaction(){
        assertEquals(0,merchant.account.getBalance(),0);
        try {
            customer.transferMoney(100,merchant);
        } catch (Exception invalidAmount) {
            invalidAmount.printStackTrace();
        }
        assertEquals(100,merchant.account.getBalance(),0);
    }
}
