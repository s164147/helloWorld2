package testing.dtu.unittests;
import CustomExceptions.TokenRequestDeniedException;
import code.TokenManager;
import org.junit.Test;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class TestTokenGenerator {
    TokenManager tkManTest = new TokenManager();

    @Test
    public void testGenerateTokens(){
        try {
            tkManTest.createTokens(5);
        } catch (TokenRequestDeniedException e) {
            e.printStackTrace();
        }
        assertEquals(11, tkManTest.getToken().getBytes().length,5);
    }
    @Test
    public void testGenerateTooMany(){
        try {
            tkManTest.createTokens(20);
        } catch (TokenRequestDeniedException e) {
            e.printStackTrace();
        }
        try{
            tkManTest.getToken();
            fail("Token should not exist because too many tokens generated.");
        }catch(NoSuchElementException e){

        }
    }

}
