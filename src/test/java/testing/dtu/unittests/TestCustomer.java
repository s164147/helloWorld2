package testing.dtu.unittests;
import CustomExceptions.TokenRequestDeniedException;
import code.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestCustomer {
    Customer customer = new Customer("Alice","1");
    TokenManager tknmngr = new TokenManager();
    Token tokens[] = new Token[5];
    @Test
    public void addAndGetTokens(){
        try {
            tknmngr.createTokens(5,customer.getCpr());
        } catch (TokenRequestDeniedException e) {
            e.printStackTrace();
        }
        for(int i = 0; i < 5; i++){
            tokens[i] = tknmngr.getToken();
        }
        customer.addToken(tokens);
        assertEquals(tokens,customer.getTokens().toArray());
    }
    @Test
    public void setAndGetName(){
        customer.setName("Bob");
        assertEquals("Bob",customer.getName());
    }
    @Test
    public void setAndGetID(){
        //No longer applicable.
        //customer.setId(5);
        //assertEquals(5,customer.getId(),0);
    }
}
