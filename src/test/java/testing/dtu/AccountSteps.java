package testing.dtu;

import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.ws.fastmoney.*;
import cucumber.api.java.After;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.List;


public class AccountSteps {


    BankService bank = new BankServiceService().getBankServicePort();
    User userCreditor = new User();
    User userDebtor = new User();
    String oleID, madsID;
    List<AccountInfo> Accounts;
    int debtorAmount = 100;
    int creditorAmount = 0;
    String errorMessage = "";
    Account testuser;
    String testAccountID;
    // bankServiceService.getBankServicePort().createAccountWithBalance(user, new BigDecimal(100));
    @Before
    public void beforeScenario() throws BankServiceException_Exception {
        userCreditor.setCprNumber("100000-1111");
        userCreditor.setFirstName("Ole");
        userCreditor.setLastName("Olsen");
        userDebtor.setLastName("Mads");
        userDebtor.setFirstName("Madsen");
        userDebtor.setCprNumber("200000-2222");

        oleID = bank.createAccountWithBalance(userCreditor, new BigDecimal(creditorAmount));
        madsID = bank.createAccountWithBalance(userDebtor, new BigDecimal(debtorAmount));
    }

    @Given("^There is two persons registered in the system with identifier \"([^\"]*)\", \"([^\"]*)\", balance (\\d+), (\\d+)$")
    public void there_is_two_persons_registered_in_the_system_with_identifier_balance(String id1, String id2, int arg1, int arg2) throws Exception {
        //already created
    }

    @When("^Identifer \"([^\"]*)\" transfers (\\d+) to account with identifier \"([^\"]*)\" and message \"([^\"]*)\"$")
    public void identiferTransfersToAccountWithIdentifierAndMessage(String arg1, int debotrAmount, String arg3, String message) throws Exception {
        bank.transferMoneyFromTo(madsID, oleID, new BigDecimal(100), message );
    }

    @Then("^Identifier \"([^\"]*)\" will have an account balance on (\\d+), and identifier \"([^\"]*)\" will have an account balance on (\\d+)$")
    public void identifierWillHaveAnAccountBalanceOnAndIdentifierWillHaveAnAccountBalanceOn(String arg1, int debtorAmount, String arg3, int creditorAmount) throws BankServiceException_Exception {
        assertEquals(debtorAmount, bank.getAccount(madsID).getBalance().intValue());
        assertEquals(creditorAmount, bank.getAccount(oleID).getBalance().intValue());
    }

    @Given("^I am registered in the bank with identifier \"([^\"]*)\" and have a balance on (\\d+)$")
    public void i_am_registered_in_the_bank_with_identifier_and_have_a_balance_on(String arg1, int arg2) throws Exception {
        //user exists already (userCreditor) (Oles current balance: 100)
    }

    @When("^I transfer (\\d+) with the  message \"([^\"]*)\"$")
    public void i_transfer_with_the_message(int amount, String message) throws Exception {
        try {
            bank.transferMoneyFromTo(madsID, null, new BigDecimal(amount), message );
        } catch (BankServiceException_Exception e) {
            errorMessage = e.getMessage();
        }
    }

    @Then("^I would get an error message with \"([^\"]*)\"$")
    public void i_would_get_an_error_message_with(String message) throws Exception {
        assertEquals(message, errorMessage);
    }

    @When("^I transfer (\\d+) to account with identifier \"([^\"]*)\"$")
    public void i_transfer_to_account_with_identifier(int arg1, String arg2) throws Exception {
        try {
            bank.transferMoneyFromTo(madsID, oleID, new BigDecimal(arg1), null );
        } catch (BankServiceException_Exception e) {
            errorMessage = e.getMessage();
        }
    }

    @Given("^I am registered in the bank and have a balance on (\\d+)$")
    public void iAmRegisteredInTheBankAndHaveABalanceOn(int amount) throws Exception {
        //user exists already (userCreditor) (Oles current balance: 100)
    }

    @When("^I transfer (\\d+) to account with identifier \"([^\"]*)\", and message \"([^\"]*)\"$")
    public void iTransferToAccountWithIdentifierAndMessage(int amount, String arg1, String message) throws Exception {
        try {
            bank.transferMoneyFromTo(null, oleID, new BigDecimal(amount), message );
        } catch (BankServiceException_Exception e) {
            errorMessage = e.getMessage();
        }

    }

    @When("^I transfer negative amount -(\\d+) to account with identifier \"([^\"]*)\", and message \"([^\"]*)\"$")
    public void iTransferNegativeAmountToAccountWithIdentifierAndMessage(int negativeAmount, String arg1, String message) throws Exception {
        try {
            bank.transferMoneyFromTo(madsID, oleID, new BigDecimal(-100), message );
        } catch (BankServiceException_Exception e) {
            errorMessage = e.getMessage();
        }
    }

    @When("^I transfer the amount (\\d+) to account with identifier \"([^\"]*)\", and message \"([^\"]*)\"$")
    public void iTransferTheAmountToAccountWithIdentifierAndMessage(int amount, String arg1, String message) throws Exception {
        try {
            bank.transferMoneyFromTo(madsID, oleID, new BigDecimal(amount), message );
        } catch (BankServiceException_Exception e) {
            errorMessage = e.getMessage();
        }
    }

    @Then("^I would get the error message \"([^\"]*)\"$")
    public void iWouldGetTheErrorMessage(String message) throws Exception {
        assertEquals(message, errorMessage);
    }

    @When("^I search for all the accounts in the bank\\.$")
    public void iSearchForAllTheAccountsInTheBank()  {
        Accounts = bank.getAccounts();
    }

    @Then("^I get a list of all the accounts in the bank\\.$")
    public void iGetAListOfAllTheAccountsInTheBank() {
        assertFalse(Accounts.isEmpty());
    }

    @When("^I search for an account with the CPR \"([^\"]*)\"$")
    public void iSearchForAnAccountWithTheCPR(String cpr) throws Exception {
        testuser = bank.getAccountByCprNumber(cpr);
    }

    @Then("^I get the account returned$")
    public void iGetTheAccountReturned() {
        assertNotNull(testuser);
    }

    @When("^I search for the non existing account with the CPR \"([^\"]*)\"$")
    public void iSearchForTheNonExistingAccountWithTheCPR(String cpr) throws Exception {
        try {
            testuser = bank.getAccountByCprNumber(cpr);
        } catch (BankServiceException_Exception e) {
            errorMessage = e.getMessage();
        }
    }

    @Then("^I get an error message with \"([^\"]*)\"$")
    public void iGetAnErrorMessageWith(String message) throws Exception {
        assertEquals(message, errorMessage);
    }

    @When("^I search for an account with the identifier \"([^\"]*)\"$")
    public void iSearchForAnAccountWithTheIdentifier(String arg0) throws Exception {
        //i search with ole's ID
        testuser = bank.getAccount(oleID);
    }

    @Then("^The account is returned$")
    public void theAccountIsReturned() {
        assertNotNull(testuser);
    }

    @When("^I search for a non existing account with the identifier \"([^\"]*)\"$")
    public void iSearchForANonExistingAccountWithTheIdentifier(String id) throws Exception {
        try {
            bank.getAccount(id);
        } catch (BankServiceException_Exception e) {
            errorMessage = e.getMessage();
        }
    }

    @Then("^I get error message \"([^\"]*)\"$")
    public void iGetErrorMessage(String message) throws Exception {
        assertEquals(errorMessage, message);
    }

    @When("^I make a new account with name \"([^\"]*)\",  lastname \"([^\"]*)\", CPR \"([^\"]*)\" and bank deposit on (\\d+)$")
    public void iMakeANewAccountWithNameLastnameCPRAndBankDepositOn(String name, String lastname, String cpr, int balance) throws Exception {
        //The user i created in the before scenario (Ole)
    }

    @Then("^the account should be added to the database$")
    public void theAccountShouldBeAddedToTheDatabase() {
        try {
            assertNotNull(bank.getAccount(oleID));
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    @When("^I make a new account with name \"([^\"]*)\",  lastname \"([^\"]*)\", same CPR \"([^\"]*)\" and bank deposit on (\\d+)$")
    public void iMakeANewAccountWithNameLastnameSameCPRAndBankDepositOn(String name, String lastname, String cpr, int balance) throws Exception {
        User accountWithExistingCpr = new User();
        accountWithExistingCpr.setCprNumber(cpr);
        accountWithExistingCpr.setFirstName(name);
        accountWithExistingCpr.setLastName(lastname);

        try {
            bank.createAccountWithBalance(accountWithExistingCpr, new BigDecimal(balance));
        } catch (BankServiceException_Exception e) {
            errorMessage = e.getMessage();
        }

    }

    @Then("^Error message occurs \"([^\"]*)\"$")
    public void errorMessageOccurs(String message) throws Throwable {
        assertEquals(errorMessage, message);
    }

    @When("^I make a new account with name \"([^\"]*)\",  lastname \"([^\"]*)\", CPR \"([^\"]*)\" and bank deposit on negative balance$")
    public void iMakeANewAccountWithNameLastnameCPRAndBankDepositOnNegativeBalance(String name, String lastname, String cpr) throws Exception {
        User account = new User();
        account.setCprNumber(cpr);
        account.setFirstName(name);
        account.setLastName(lastname);

        try {
            bank.createAccountWithBalance(account, new BigDecimal(-100));
        } catch (BankServiceException_Exception e) {
            errorMessage = e.getMessage();
        }
    }

    @Then("^I would get \"([^\"]*)\"$")
    public void iWouldGet(String message) throws Throwable {
        assertEquals(errorMessage, message);
    }

    @When("^I delete an existing account$")
    public void iDeleteAnExistingAccount() {
        try {
            bank.retireAccount(oleID);
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
        }
    }

    @Then("^I should get \"([^\"]*)\"$")
    public void iShouldGet(String message) throws Exception {
        try {
            bank.getAccount(oleID);
        } catch (BankServiceException_Exception e) {
            errorMessage = e.getMessage();
        }
        assertEquals(errorMessage, message);
    }

    @When("^I delete a non existing account$")
    public void iDeleteANonExistingAccount() {
        try {
            bank.retireAccount("noAccount");
        } catch (BankServiceException_Exception e) {
            errorMessage = e.getMessage();
        }
    }

    @Then("^I get error message with \"([^\"]*)\"$")
    public void iGetErrorMessageWith(String message) throws Throwable {
        assertEquals(errorMessage, message);
    }

    @After
    public void afterScenario() throws BankServiceException_Exception {
        try {
            bank.retireAccount(oleID);
        } catch (BankServiceException_Exception e) {
        }

        try {
            bank.retireAccount(madsID);
        } catch (BankServiceException_Exception e) {
        }
        /*for(int i = 0; i < bank.getAccounts().size(); i++) {
            System.out.println(bank.getAccounts().get(i).getUser().getFirstName() + " " + bank.getAccounts().get(i).getAccountId());
        }*/
        //System.out.println(bank.getAccounts().get(0).getUser().getFirstName() + " " + bank.getAccounts().get(0).getAccountId());
        //bank.retireAccount("6d451f4d-957b-4a68-8ec9-d27f92080603");
    }
}