package testing.dtu;

import CustomExceptions.TokenRequestDeniedException;
import code.Customer;
import code.DatabaseController;
import code.TokenManager;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.ws.fastmoney.*;
import org.junit.Before;

import java.math.BigDecimal;
import java.util.LinkedList;

import static org.junit.Assert.*;

public class CustomerSteps {

    String[] tokenList = {"a","b","c","d","5"};
    Customer customer = new Customer("bob", "7");
    DatabaseController db = DatabaseController.getInstance();
    Customer a = new Customer("Joe","1");
    Customer b = new Customer("Bob","2");
    Customer c = new Customer("Alice","3");

    TokenManager tokenManager = new TokenManager();

    Customer customerList[] = {a,b,c};

    @Given("^I am created in the system with the uniqID of (\\d+)$")
    public void i_am_created_in_the_system_with_the_uniqID_of(String arg1) {
        db.addUser(a);
        assertNotNull(db.findUser(arg1));
    }


    @Given("^I have (\\d+) unused tokens$")
    public void i_have_unused_tokens(int arg1) {
       // assertTrue(db.findUser(a.getCpr()).getTokens().size() == arg1);
    }

    @When("^I request (\\d+) tokens$")
    public void iRequestTokens(int arg1) throws TokenRequestDeniedException {
       // String[] token = tokenManager.createTokens(arg1).toArray(new String[arg1]);
       // db.findCustomer(a.getId()).addToken(token);
        throw new PendingException();
    }

    @Then("^I should have (\\d+) tokens$")
    public void iShouldHaveTokens(int arg1)  {

        //assertTrue(db.findCustomer((a.getId())).getTokens().size() == arg1);
    }

    @Given("^I have more than (\\d+) unused tokens$")
    public void iHaveMoreThanUnusedTokens(int arg1)  {
        //db.findCustomer(a.getId()).addToken(tokenList);
        //assertTrue(db.findCustomer(a.getId()).getTokens().size() > arg1);
        throw new PendingException();
    }

    @Then("^I should get a TokenRequestDenied error message \"([^\"]*)\"$")
    public void iShouldGetATokenRequestDeniedErrorMessage(String arg1) throws Exception {
       tokenManager.setTokens(a.getTokens());
       try {
           tokenManager.createTokens(1,"testUserID");
       }
       catch (TokenRequestDeniedException e){
           assertEquals(e.getMessage(),arg1);
       }
    }

    @Given("^I have less than (\\d+) unused tokens$")
    public void iHaveLessThanUnusedTokens(int arg1) throws Exception {
        //assertTrue(db.findCustomer(a.getId()).getTokens().size() < arg1);
    }



}
