package testing.dtu;
import code.Merchant;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.ws.fastmoney.*;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class refundSteps {
    BankService bank = new BankServiceService().getBankServicePort();
    User userCreditor = new User();
    User userDebtor = new User();
    String jensID, carstenID;
    List<AccountInfo> Accounts;
    int debtorAmount = 100;
    int creditorAmount = 0;
    String errormessage = "Fejlbesked";

    // bankServiceService.getBankServicePort().createAccountWithBalance(user, new BigDecimal(100));
    @Before
    public void beforeScenario() throws BankServiceException_Exception {
        userCreditor.setCprNumber("110000-1111");
        userCreditor.setFirstName("Jens");
        userCreditor.setLastName("Jensen");
        userDebtor.setLastName("Karsten");
        userDebtor.setFirstName("Karstensen");
        userDebtor.setCprNumber("220000-2222");

        jensID = bank.createAccountWithBalance(userCreditor, new BigDecimal(creditorAmount));
        carstenID = bank.createAccountWithBalance(userDebtor, new BigDecimal(debtorAmount));

    }


    @Given("^The merchant exist$")
    public void the_merchant_exist() throws Exception {
        System.out.println("Merchant exists");
/*
        int i = 1;
        System.out.println(bank.getAccounts().get(i).getUser().getFirstName() + " "
                + bank.getAccounts().get(i).getUser().getLastName()
                +" "+ bank.getAccounts().get(i).getAccountId());
  */
    }

    @Given("^The presented token is valid$")
    public void the_presented_token_is_valid() throws Exception {
        System.out.println("The Token is valid");

    }
    @When("^The customer requests a refund$")
    public void the_customer_requests_a_refund() throws Exception {
        bank.transferMoneyFromTo(carstenID, jensID, new BigDecimal(100), "Money refunded");
    }

    @Then("^The customer will be refunded$")
    public void the_customer_will_be_refunded() throws Exception {
        assertEquals(debtorAmount, bank.getAccount(carstenID).getBalance().intValue());
        assertEquals(creditorAmount, bank.getAccount(jensID).getBalance().intValue());
    }

    @Given("^The merchant no longer exists$")
    public void the_merchant_no_longer_exists() throws Exception {
        bank.retireAccount(jensID);
        assertEquals(null, bank.getAccount(jensID));
    }

    @Then("^The customer will not be refunded$")
    public void the_customer_will_not_be_refunded() throws Exception {
        System.out.println("jens balance: " + bank.getAccount(jensID).getBalance().intValue());
        System.out.println("jens balance: " + bank.getAccount(carstenID).getBalance().intValue());


        assertEquals(creditorAmount, bank.getAccount(jensID).getBalance().intValue());
        assertEquals(debtorAmount, bank.getAccount(carstenID).getBalance().intValue());
    }

    @Given("^The token is invalid$")
    public void the_token_is_invalid() throws Exception {
        try {
            bank.getAccount(jensID);
        } catch (BankServiceException_Exception e) {
            errormessage = e.getMessage();
        }
    }

    @After
    public void afterScenario() throws BankServiceException_Exception {
        bank.retireAccount(jensID);
        bank.retireAccount(carstenID);
    }
}
