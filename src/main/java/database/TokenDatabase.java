package database;

import code.Token;

import java.util.LinkedList;

public class TokenDatabase {

    private LinkedList<Token> tokens;

    public TokenDatabase(){
        tokens = new LinkedList<>();
    }

    public void addToken(Token token){
        tokens.add(token);
    }

    public boolean tokenExists(String searchTerm){
        for (Token token :
                tokens) {
            if(token.getTokenString().equals(searchTerm)){
                return true;
            }
            else continue;
        }
        return false;
    }

    public void removeToken(Token searchTerm){
        tokens.remove(searchTerm);
    }
}
