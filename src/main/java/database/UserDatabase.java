package database;
import code.*;

import java.util.LinkedList;
import java.util.Optional;

public class UserDatabase {

    private LinkedList<User> list;

    public UserDatabase(){
        list = new LinkedList<>();
    }

    public void add(User user){
        list.add(user);
    }

    public Optional<User> findUser(final String searchTerm){
        return list.stream().filter(c -> c.getCpr().equals(searchTerm)).findAny();
    }

    public void removeUser(final String searchTerm){
        list.removeIf(p -> (findUser(searchTerm).isPresent()));
    }

}
