package rest;

import code.DatabaseController;

import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.awt.*;

//Needs to interact with SOAP for bank contact.
@Path("/transfer")

public class TransferMoneyEndpoint {
    DatabaseController DBController = DatabaseController.getInstance();

    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    public Response handleTransfer(HandleTransferBody handleTransferBody){
        int moneyToTransfer = handleTransferBody.amount;
        //Check if token is legit
        if(DBController.tokenExists(handleTransferBody.token)){
            //Needs to check for the customerID, not 2. CustomerID is string, not int. Database needs changing.
            if(DBController.findUser("2") != null){
                //SOAP bank request forwarded with sender, receiver and money amount.
                return Response.ok("OK.").build();
            }
            return Response.ok("Error: You do not exist. Please exist, then try again.").build();
        }
        return Response.ok("Error: invalid token").build();
    }
}

@XmlRootElement
class HandleTransferBody {
    @XmlElement int amount;
    @XmlElement String sender;
    @XmlElement String receiver;
    @XmlElement String token;
}