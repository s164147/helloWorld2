package rest;

import CustomExceptions.TokenRequestDeniedException;
import code.Customer;
import code.DatabaseController;
import code.TokenManager;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

//For security, there should be a mechanism to authenticate the identity of the client making the request.
@Path("/getToken{tk}{customer}")
public class GetTokenEndpoint {
    DatabaseController DBcontroller = DatabaseController.getInstance();
    TokenManager tempTokenManager = new TokenManager();
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTokenRestResponse(@PathParam("tk") int tokenCount, @PathParam("customer") String customerCPR){
        DBcontroller.addUser(new Customer("Joe","1010"));
        //Check if customer exists in database.
        String tokens[];
        if(DBcontroller.findUser(customerCPR) != null){
            try {
                tokens = tempTokenManager.createTokens(tokenCount).toArray(new String[0]);
            } catch (TokenRequestDeniedException e) {
                return Response.noContent().build();
            }
            return Response.ok(tokens).build();
        }
        return Response.noContent().build();
    }
}
