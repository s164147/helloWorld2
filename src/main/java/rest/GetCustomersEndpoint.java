package rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import code.*;
@Path("/getCustomers/{cpr}")
public class GetCustomersEndpoint {
    DatabaseController dbc = DatabaseController.getInstance();

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomer(@PathParam("cpr") String cpr) {
        User customer = dbc.findUser(cpr);
        if(customer == null || !(customer instanceof Customer)) return Response.ok("No customer of that ID found.").build();
        return Response.ok(customer).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addCustomer(AddCustomerBody addCustomerBody){
        Customer customer = new Customer(addCustomerBody.name,addCustomerBody.cpr);
        //Temp. Customer should be initialized with tokens.
        customer.addToken(new String[]{"a", "b", "c"});
        dbc.addUser(customer);
        return Response.accepted("Added Customer.").build();
    }
}

@XmlRootElement
class RequestBody {
    @XmlElement String cpr;
}

@XmlRootElement
class AddCustomerBody {
    @XmlElement String cpr;
    @XmlElement String name;
}
