package code;

public class Account {
    public Account(float money){
        this.money = money;
    }
    float money;


    public void addMoney(float amount) throws invalidAmount {
        if(amount < 1){
            throw new invalidAmount("Must add positive amount over 0");
        }
        money = money + amount;
    }

    public void removeMoney(float amount) throws invalidAmount {
        if(amount < 1){
            throw new invalidAmount("Must remove positive amount.");
        }
        money = money - amount;
    }

    public float getBalance(){
        return money;
    }
}

class invalidAmount extends Exception{
    public invalidAmount(String errorMessage) {
        super(errorMessage);
    }
}

