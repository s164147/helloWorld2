package code;

import java.security.SecureRandom;
import java.util.Collections;
import java.util.*;

public class Customer extends User{

    public Account account = new Account(100);
    private LinkedList<Token> tokens = new LinkedList<Token>();

    public Customer(String name, String cpr) {
        super(name, cpr);
    }


    public Token getToken() throws NoSuchElementException {
        return tokens.removeFirst();
    }

    public void setTokens(LinkedList<Token> tokens) {
        this.tokens = tokens;
    }


    public LinkedList<Token> getTokens() {
        return tokens;
    }


    public void addToken(Token[] tokensToAdd) {
        Collections.addAll(tokens,tokensToAdd);
    }

    public void transferMoney(float amount, Merchant recipient) throws invalidAmount {
        account.removeMoney(amount);
        recipient.account.addMoney(amount);
    }
}
