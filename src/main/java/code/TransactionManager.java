package code;

import java.util.List;

public class TransactionManager {
    List<Transactions> customerTransactionLog;
    List<Transactions> merchantTransactionLog;

    public List<Transactions> getCustomerTransactionLog() {
        return customerTransactionLog;
    }

    public void setMerchantTransactionLog(List<Transactions> merchantTransactionLog) {
        this.merchantTransactionLog = merchantTransactionLog;

    }
}
