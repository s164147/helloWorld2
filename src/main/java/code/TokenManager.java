package code;

import CustomExceptions.TokenRequestDeniedException;
import com.mifmif.common.regex.Generex;

import java.security.SecureRandom;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class TokenManager {

    public TokenManager() {
    }

    private int tokenSize = 20;

    public void setTokens(LinkedList<Token> tokens) {
        this.tokens = tokens;
    }

    private LinkedList<Token> tokens = new LinkedList<>();

    public LinkedList<Token> createTokens(int request, String userID) throws TokenRequestDeniedException {
        if (tokens.size() < 2 && tokens.size() + request < 6) {
            for (int i = 0; i < request; i++) {
                tokens.add(generateToken(userID));
            }
            return tokens;
        } else {
            if (tokens.size() > 2) {
                throw new TokenRequestDeniedException(
                        "You can't request tokens, while having more than 1 avaible");
            } else if (request > 6) {
                throw new TokenRequestDeniedException(
                        "Token size is big");
            } else if (request < 0) {
                throw new TokenRequestDeniedException(
                        "Token size is big");
            }
            return null;
        }
    }


    //TODO: Prevent duplicate tokens!
    private Token generateToken(String userID) {
        Generex generex = new Generex("[a-zA-Z0-9]{10}");
        String tk = generex.random();
        Token token = new Token(userID,tk);
        System.out.println(token);
        return token;
    }

    public Token getToken() throws NoSuchElementException {
        return tokens.removeFirst();
    }

}
