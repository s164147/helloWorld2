package code;

public class Token {
    private String userID;
    private String tokenString;

    public Token(String userID, String tokenString){
        this.userID = userID;
        this.tokenString = tokenString;
    }

    public String getUserID() {
        return userID;
    }

    public String getTokenString() {
        return tokenString;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
