package code;
import database.*;

public class DatabaseController {

    private static DatabaseController instance = null;
    private UserDatabase userDatabase;
    private TokenDatabase tokenDatabase;

    public static DatabaseController getInstance(){
        if(instance == null){
            instance = new DatabaseController();
        }
        return instance;
    }

    private DatabaseController(){
        userDatabase = new UserDatabase();
        tokenDatabase = new TokenDatabase();
    }

    public void addUser(User user){
        userDatabase.add(user);
    }

    public void removeUser(String searchTerm)
    {
        userDatabase.removeUser(searchTerm);
    }

    public User findUser(String searchTerm){
        return userDatabase.findUser(searchTerm).orElse(null);
    }

    public void addToken(Token token){
        tokenDatabase.addToken(token);
    }

    public void removeToken(Token token){
        tokenDatabase.removeToken(token);
    }

    public boolean tokenExists(String token){
        return tokenDatabase.tokenExists(token);
    }
}
