package CustomExceptions;

public class TokenRequestDeniedException extends Exception {
    public TokenRequestDeniedException(String errorMsg) {
        super(errorMsg);
    }
}
